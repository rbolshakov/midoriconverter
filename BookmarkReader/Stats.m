//
//  Stats.m
//  BookmarkReader
//
//  Created by Nestline on 07/10/2016.
//
//

#import "Stats.h"

@interface Stats()
@property (nonatomic, strong) NSMutableArray* codes;
@property (nonatomic, strong) NSMutableArray* kanjis;
@end

@implementation Stats
- (instancetype)initWithFolder:(Folder*)folder
{
    if (self = [super init])
    {
        [self clearArrays];
        [self fillStatsFromFolder:folder];
        [self processResults];
    }
    return self;
}

- (void)clearArrays
{
    self.codes = [NSMutableArray array];
    self.kanjis = [NSMutableArray array];
    self.folders = 0;
}

- (void)processResults
{
    self.totalWords = self.codes.count;
    self.totalKanjis = self.kanjis.count;
    self.uniqueWords = [NSSet setWithArray:self.codes].count;
    self.uniqueKanjis = [NSSet setWithArray:self.kanjis].count;
}

- (void)fillStatsFromFolder:(Folder*)folder
{
    for (Bookmark* bookmark in folder.items)
    {
        if ([bookmark isKindOfClass:[Word class]])
        {
            Word* word = (Word*)bookmark;
            if (word.isKanji)
            {
                [self.kanjis addObject:word.code];
            }
            else
            {
                [self.codes addObject:word.code];
            }
        }
        else
        {
            Folder* subFolder = (Folder*)bookmark;
            self.folders++;
            [self fillStatsFromFolder:subFolder];
        }
    }
}


@end
