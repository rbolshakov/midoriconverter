//
//  CSVParser.h
//  BookmarkReader
//
//  Created by Nestline on 10/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "DBOperator.h"
#import "Folder.h"
#import "Word.h"

@protocol CSVParserDelegate <NSObject>
- (void)didProcessed:(NSInteger)words;
- (void)couldNotImportWords:(NSArray*)strings;
@end

@interface CSVParser : NSObject
@property (nonatomic, weak) id<CSVParserDelegate> delegate;
- (Folder*)parseString:(NSString*)string;
@end
