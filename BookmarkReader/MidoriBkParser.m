//
//  BookmarksLoader.m
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import "MidoriBkParser.h"

@interface MidoriBkParser()
{
    int totalWords;
}
@end

@implementation MidoriBkParser
- (NSString*)serializeBookmarks:(Folder*)rootBookmark
{
    NSMutableDictionary* rootDictionary = [NSMutableDictionary dictionary];
    rootDictionary[@"version"] = @1;
    rootDictionary[@"isroot"] = @1;
    NSMutableDictionary* bookmarkDic = [self JSONDicWithFolder:rootBookmark];
    rootDictionary[@"data"] = bookmarkDic;
    NSError* error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rootDictionary options:kNilOptions error:&error];
    NSString* bookmarks = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return [self removeColonsFromJS:bookmarks];
}

- (Folder*)deserializeBookmarks:(NSString*)bookmarks
{
    NSString* processedBookmarks = [self addColonsToJS:bookmarks];
    NSData* jsonData = [processedBookmarks dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if (error)
        NSLog(@"Error: %@", error);
    totalWords = 0;
    Folder* rootFolder = [self folderWithJSONDic:json[@"data"]];
    rootFolder.text = @"Bookmarks";
    return rootFolder;
}

- (NSString*)removeColonsFromJS:(NSString*)string
{
    NSString* processedString = [string copy];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"version\":"
                                                                 withString:@"version:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"data\":"
                                                                 withString:@"data:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"isroot\":"
                                                                 withString:@"isroot:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"n\":"
                                                                 withString:@"n:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"f\":"
                                                                 withString:@"f:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"b\":"
                                                                 withString:@"b:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"t\":"
                                                                 withString:@"t:"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"\"i\":"
                                                                 withString:@"i:"];
    return processedString;
}

- (NSString*)addColonsToJS:(NSString*)string
{
    NSString* processedString = [string copy];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"version:"
                                                                 withString:@"\"version\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"data:"
                                                                 withString:@"\"data\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"isroot:"
                                                                 withString:@"\"isroot\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"n:"
                                                                 withString:@"\"n\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"f:"
                                                                 withString:@"\"f\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"b:"
                                                                 withString:@"\"b\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"t:"
                                                                 withString:@"\"t\":"];
    processedString = [processedString stringByReplacingOccurrencesOfString:@"i:"
                                                                 withString:@"\"i\":"];
    return processedString;
}

- (NSMutableDictionary*)JSONDicWithFolder:(Folder*)folder
{
    NSMutableDictionary* jsonDic = [NSMutableDictionary dictionary];
    jsonDic[@"n"] = folder.text;
    NSMutableArray* jsonWords = [NSMutableArray array];
    NSMutableArray* jsonFolders = [NSMutableArray array];
    for (NSObject* bookmark in folder.items)
    {
        if ([bookmark isKindOfClass:[Folder class]])
        {
            Folder* folder = (Folder*)bookmark;
            NSMutableDictionary* jsonFolder = [self JSONDicWithFolder:folder];
            [jsonFolders addObject:jsonFolder];
        }
        else if ([bookmark isKindOfClass:[Word class]])
        {
            Word* word = (Word*)bookmark;
            NSMutableDictionary* jsonWord = [NSMutableDictionary dictionary];
            jsonWord[@"t"] = @((int)word.isKanji);
            jsonWord[@"i"] = word.code;
            [jsonWords addObject:jsonWord];
        }
    }
    if (jsonWords.count > 0)
        jsonDic[@"b"] = jsonWords;
    if (jsonFolders.count > 0)
        jsonDic[@"f"] = jsonFolders;
    return jsonDic;
}

- (Folder*)folderWithJSONDic:(NSDictionary*)jsonDic
{
    Folder* topFolder = [[Folder alloc]init];
    topFolder.text = jsonDic[@"n"];
    if ([jsonDic.allKeys containsObject:@"f"])
    {
        NSArray* jsonFolders = jsonDic[@"f"];
        for (NSDictionary* jsonFolder in jsonFolders)
        {
            Folder* folder = [self folderWithJSONDic:jsonFolder];
            [topFolder addBookmark:folder];
        }
    }
    if ([jsonDic.allKeys containsObject:@"b"])
    {
        NSArray* jsonWords = jsonDic[@"b"];
        for (NSDictionary* jsonWord in jsonWords)
        {
            Word* word;
            BOOL isKanji = [jsonWord[@"t"]boolValue];
            if (!isKanji)
            {
                word = [[DBOperator sharedOperator] findWordDbByCode:jsonWord[@"i"]];
            }
            else
            {
                word = [[DBOperator sharedOperator] findKanjiDb:jsonWord[@"i"]];
            }
            if (word)
                [topFolder addBookmark:word];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                totalWords++;
                [self.delegate didProcessed:totalWords];
            });
        }
    }
    return topFolder;
}
@end
