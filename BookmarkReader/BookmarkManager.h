//
//  BookmarkManager.h
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "Folder.h"
#import "Word.h"
#import "Bookmark.h"

@interface BookmarkManager : NSObject
- (void)mergeBookmarks:(NSArray*)fromItems toBookmarks:(NSArray*)toItems;
- (void)substractBookmarks:(NSArray*)toItems fromBookmarks:(NSArray*)fromItems;
- (void)insertBookmarks:(NSArray*)fromItems toBookmarks:(NSArray*)toItems;
- (void)splitBookmarks:(NSArray*)bookmarks by:(NSInteger)count;
- (void)deleteBookmarks:(NSArray*)bookmarks;
- (void)createFolderInBookmark:(Bookmark*)bookmark;
- (NSString*)descriptionFromBookmarks:(NSMutableArray*)bookmarks;
@end
