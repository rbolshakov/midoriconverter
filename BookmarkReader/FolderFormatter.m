//
//  FolderFormatter.m
//  BookmarkReader
//
//  Created by Nestline on 12/10/2016.
//
//

#import "FolderFormatter.h"
#import "Folder.h"

@implementation FolderFormatter
- (nullable NSString *)stringForObjectValue:(nullable id)obj
{
    Folder* folder = (Folder*)obj;
    return [NSString stringWithFormat:@"%@ (%ld) [%ld]", folder.text, folder.items.count, folder.totalWords];
}

- (nullable NSString *)editingStringForObjectValue:(id)obj
{
    Folder* folder = (Folder*)obj;
    return [NSString stringWithFormat:@"%@", folder.text];
}

- (BOOL)getObjectValue:(out id _Nullable * _Nullable)obj forString:(NSString *)string errorDescription:(out NSString * _Nullable * _Nullable)error
{
    Folder* folder = [[Folder alloc]init];
    folder.text = string;
    *obj = folder;
    return YES;
}

@end
