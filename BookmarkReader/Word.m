//
//  Word.m
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import "Word.h"

@implementation Word
- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[Word class]])
    {
        Word * other = object;
        return [self.code isEqualToString:other.code];
    }
    else
    {
        return NO;
    }
}

-(id)copyWithZone:(NSZone *)zone
{
    Word *another = (Word*)[super copyWithZone:zone];
    another.code = [self.code copyWithZone:zone];
    another.isKanji = self.isKanji;
    return another;
}
@end
