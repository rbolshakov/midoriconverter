//
//  DBOperator.h
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "Word.h"
#import "Folder.h"
#import "Bookmark.h"
#import "DBManager.h"

@interface DBOperator : NSObject
+ (instancetype)sharedOperator;
- (Word*)findKanjiDb:(NSString*)kanji;
- (Word*)findWordDbByCode:(NSString*)code;
- (NSArray*)findWordsDbByText:(NSString*)text;
@end
