//
//  CSVParser.m
//  BookmarkReader
//
//  Created by Nestline on 10/10/2016.
//
//

#import "CSVParser.h"
#import "DBOperator.h"

@implementation CSVParser
- (Folder*)parseString:(NSString *)string
{
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    Folder* folder = [[Folder alloc]init];
    folder.text = @"Import";
    NSArray *rows = [trimmedString componentsSeparatedByString:@"\n"];
    
    
    NSMutableArray *failedWords = [NSMutableArray array];
    int i = 0;
    for (NSString* row in rows)
    {
        NSArray *words = [row componentsSeparatedByString:@","];
        NSString *searchText = [self stringByTrimmingSearchString:[words firstObject]];
        NSArray* foundWords = [[DBOperator sharedOperator] findWordsDbByText:searchText];
        for (Word* word in foundWords)
        {
            [folder addBookmark:word];
        }
        if (words.count == 0)
        {
            NSLog(@"Could not import: %@", searchText);
            [failedWords addObject: [words firstObject]];
        }
        i++;
        [self.delegate didProcessed:i];
    }
    if (failedWords.count > 0)
        [self.delegate couldNotImportWords:failedWords];
    
    return folder;
}

- (NSString*)stringByTrimmingSearchString:(NSString*)string
{
    NSCharacterSet* set = [NSCharacterSet characterSetWithCharactersInString:@"\""];
    NSString* searchText = [string stringByTrimmingCharactersInSet:set];
    set = [NSCharacterSet characterSetWithCharactersInString:@"～"];
    searchText = [string stringByTrimmingCharactersInSet:set];
    searchText = [[searchText componentsSeparatedByString:@"/"] firstObject];
    return searchText;
}

@end
