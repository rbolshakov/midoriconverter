//
//  Stats.h
//  BookmarkReader
//
//  Created by Nestline on 07/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "Bookmark.h"
#import "Word.h"
#import "Folder.h"

@interface Stats : NSObject
@property (nonatomic) NSInteger uniqueWords;
@property (nonatomic) NSInteger uniqueKanjis;
@property (nonatomic) NSInteger totalWords;
@property (nonatomic) NSInteger totalKanjis;
@property (nonatomic) NSInteger folders;

- (instancetype)initWithFolder:(Folder*)folder;

@end
