//
//  Bookmark.m
//  BookmarkReader
//
//  Created by Nestline on 07/10/2016.
//
//

#import "Bookmark.h"
#import "Folder.h"

@implementation Bookmark
- (void)deleteIt
{
    [self.parent.items removeObject:self];
}

-(id)copyWithZone:(NSZone *)zone
{
    Bookmark *another = [[[self class] allocWithZone:zone] init];
    another.text = [self.text copyWithZone:zone];
    another.meaning = [self.meaning copyWithZone:zone];
    another.pronounciation = [self.pronounciation copyWithZone:zone];
    another.parent = nil;
    return another;
}

@end
