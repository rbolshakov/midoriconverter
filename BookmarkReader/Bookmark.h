//
//  Bookmark.h
//  BookmarkReader
//
//  Created by Nestline on 07/10/2016.
//
//

#import <Foundation/Foundation.h>
@class Folder;

@interface Bookmark : NSObject<NSCopying>
@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) NSString* pronounciation;
@property (nonatomic, strong) NSString* meaning;
@property (nonatomic, weak) Folder* parent;
- (void)deleteIt;
@end
