//
//  BookmarkGroup.h
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "Bookmark.h"
@class Word;

@interface Folder : Bookmark<NSCopying>
@property (nonatomic, strong) NSMutableArray* items;
- (void)addBookmark:(Bookmark*)bookmark;
- (NSInteger)totalWords;
@end
