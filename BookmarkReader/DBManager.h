//
//  DBManager.h
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject
+ (instancetype)sharedManager;
-(NSArray *)loadDataFromDB:(NSString *)query;
-(void)executeQuery:(NSString *)query;
@end
