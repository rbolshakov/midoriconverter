//
//  BookmarksLoader.h
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import <Foundation/Foundation.h>
#import "Folder.h"
#import "Word.h"
#import "DBOperator.h"

@protocol BkParserDelegate <NSObject>
- (void)didProcessed:(NSInteger)words;
@end

@interface MidoriBkParser : NSObject
@property (nonatomic, weak) id<BkParserDelegate> delegate;

- (NSString*)serializeBookmarks:(Folder*)rootBookmark;
- (Folder*)deserializeBookmarks:(NSString*)bookmarks;
- (NSString*)removeColonsFromJS:(NSString*)string;
- (NSString*)addColonsToJS:(NSString*)string;
- (NSMutableDictionary*)JSONDicWithFolder:(Folder*)folder;
- (Folder*)folderWithJSONDic:(NSDictionary*)jsonDic;
@end
