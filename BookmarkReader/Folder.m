//
//  BookmarkGroup.m
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import "Folder.h"
#import "Word.h"
@interface Folder()

@end

@implementation Folder
- (id)init
{
    if (self = [super init])
    {
        self.items = [NSMutableArray array];
    }
    return self;
}

- (void)addBookmark:(Bookmark *)bookmark
{
    [self.items addObject:bookmark];
    bookmark.parent = self;
}

-(id)copyWithZone:(NSZone *)zone
{
    Folder *another = [super copyWithZone:zone];
    another.items = [NSMutableArray array];
    for (Bookmark* item in self.items)
    {
        Bookmark* copy = [item copyWithZone:zone];
        [another addBookmark:copy];
    }
    return another;
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[Folder class]])
    {
        Folder* other = object;
        return [other.text isEqualToString:self.text] && other.items.count == self.items.count;
    }
    else
    {
        return NO;
    }
}

-(NSInteger)totalWords
{
    int total = 0;
    for (Bookmark* bookmark in self.items)
    {
        if ([bookmark isKindOfClass:[Word class]])
        {
            total++;
        }
        else
        {
            Folder* folder = (Folder*)bookmark;
            total += [folder totalWords];
        }
    }
    return total;
}

@end
