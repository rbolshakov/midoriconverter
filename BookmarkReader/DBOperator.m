//
//  DBOperator.h
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import "DBOperator.h"
#import "DBManager.h"
#import "Word.h"
#import "Bookmark.h"

@implementation DBOperator

+ (instancetype)sharedOperator
{
    static DBOperator *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (Word*)findWordDbByCode:(NSString*)code
{
    Word* word;
    NSString* query = [NSString stringWithFormat:@"SELECT * FROM entry WHERE id = %@", code];
    NSArray* arrayFromDb = [[DBManager sharedManager] loadDataFromDB:query];
    NSArray* row = [arrayFromDb firstObject];
    if (row)
    {
       word  = [self wordFromDbRow:row];
    }
    return word;
}

- (Word*)findKanjiDb:(NSString *)kanji
{
    Word* word;
    NSString* query = [NSString stringWithFormat:@"SELECT * FROM kanji WHERE literal = \"%@\"", kanji];
    NSArray* arrayFromDb = [[DBManager sharedManager] loadDataFromDB:query];
    NSArray* row = [arrayFromDb firstObject];
    if (row)
    {
        word  = [self kanjiFromDbRow:row];
    }
    return word;
}

- (NSArray*)findWordsDbByText:(NSString*)text
{
    NSMutableArray* words = [[NSMutableArray alloc]init];
    NSString* query = [NSString stringWithFormat:@"SELECT * FROM entry WHERE word1 like '%%%@%%'", text];
    NSArray* arrayFromDb = [[DBManager sharedManager] loadDataFromDB:query];
    for (NSArray* row in arrayFromDb)
    {
        if ([self fieldText:row[1] isCorrectFor:text])
        {
            Word* word = [self wordFromDbRow:row];
            [words addObject:word];
        }
    }
    return words;
}

- (Word*)wordFromDbRow:(NSArray*)row
{
    Word* word = [[Word alloc]init];
    word.isKanji = NO;
    word.code = row[0];
    NSString* text = row[1];
    if ([text isEqualToString:@""]) //Word has no kanji, so show hiragana
    {
        text = row[2];
    }
    word.text = [self stringByTrimmingDbString:text];
    NSString* meaning = row[3];
    word.meaning = [self stringByTrimmingDbString:meaning];
    NSString* pronounciation = row[2];
    word.pronounciation = [self stringByTrimmingDbString:pronounciation];
    return word;
}

- (Word*)kanjiFromDbRow:(NSArray*)row
{
    Word* word = [[Word alloc]init];
    word.isKanji = YES;
    word.code = row[0];
    NSString* frequency = row[1];
    NSString* jlpt = row[3];
    NSMutableString* text = [NSMutableString stringWithFormat:@"%@(%@)", word.code, frequency];
    if (![jlpt isEqualToString:@""])
    {
        [text appendString:[NSString stringWithFormat:@" [%@]", jlpt]];
    }
    word.text = text;
    NSString* meaning = row[4];
    word.meaning = [self stringByTrimmingDbString:meaning];
    NSString* pronounciationOn = row[5];
    NSString* pronounciationKun = row[6];
    word.pronounciation = [NSString stringWithFormat:@"%@ | %@", [self stringByTrimmingDbString:pronounciationKun], [self stringByTrimmingDbString:pronounciationOn]];
    return word;
}

- (BOOL)fieldText:(NSString*)foundText isCorrectFor:(NSString*)searchText
{
    BOOL startIsCorrect;
    BOOL endIsCorrect;
    
    NSRange foundRange = [foundText rangeOfString:searchText];
    while(foundRange.location != NSNotFound)
    {

        if (foundRange.location == 0)
            startIsCorrect = YES;
        else
        {
            NSString *previousSymbol = [foundText substringWithRange:NSMakeRange(foundRange.location - 1, 1)];
            if ([previousSymbol isEqualToString:@"{"])
                startIsCorrect = YES;
            else
                startIsCorrect = NO;
        }
        
        if (foundRange.location + foundRange.length == foundText.length)
            endIsCorrect = YES;
        else
        {
            NSString *nextSymbol = [foundText substringWithRange:NSMakeRange(foundRange.location + foundRange.length, 1)];
            if ([nextSymbol isEqualToString:@"{"] || [nextSymbol isEqualToString:@"}"])
                endIsCorrect = YES;
            else
                endIsCorrect = NO;
        }
        
        if (startIsCorrect && endIsCorrect)
            return YES;
        
        foundRange = [foundText rangeOfString:searchText options:0 range:NSMakeRange(foundRange.location + 1, [foundText length] - foundRange.location - 1)];
    }
    return NO;
}

- (NSString*)stringByTrimmingDbString:(NSString*)string
{
    NSRange range = [string rangeOfString:@"{"];
    if (range.location != NSNotFound)
    {
        string = [string substringToIndex:range.location];
    }
    
    range = [string rangeOfString:@"}"];
    if (range.location != NSNotFound)
    {
        string = [string substringToIndex:range.location];
    }
    return string;
}

@end
