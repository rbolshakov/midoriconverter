//
//  ViewController.h
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import <Cocoa/Cocoa.h>
#import "Word.h"
#import "Folder.h"
#import "DBManager.h"
#import "CSVParser.h"
#import "MidoriBkParser.h"


@interface ViewController : NSViewController<NSOutlineViewDataSource, NSOutlineViewDelegate, CSVParserDelegate, BkParserDelegate>
@property (nonatomic, strong) IBOutlet NSOutlineView* outlineView;
@property (weak) IBOutlet NSButton *fromButton;
@property (weak) IBOutlet NSButton *toButton;
@property (weak) IBOutlet NSButton *readButton;
@property (weak) IBOutlet NSButton *writeButton;
@property (weak) IBOutlet NSButton *importButton;
@property (weak) IBOutlet NSButton *mergeButton;
@property (weak) IBOutlet NSButton *substractButton;
@property (weak) IBOutlet NSButton *insertButton;
@property (weak) IBOutlet NSButton *splitButton;
@property (weak) IBOutlet NSButton *deleteButton;
@property (weak) IBOutlet NSButton *createButton;
@property (weak) IBOutlet NSButton *statsButton;

@property (weak) IBOutlet NSTextField *fromField;
@property (weak) IBOutlet NSTextField *toField;
@property (weak) IBOutlet NSTextField *logField;
@property (weak) IBOutlet NSTextField *splitField;

- (IBAction)openFile:(id)sender;
- (IBAction)openImportFile:(id)sender;
- (IBAction)saveFile:(id)sender;

- (IBAction)selectFrom:(id)sender;
- (IBAction)selectTo:(id)sender;

- (IBAction)insertBookmarks:(id)sender;
- (IBAction)mergeBookmarks:(id)sender;
- (IBAction)substractBookmarks:(id)sender;
- (IBAction)deleteBookmarks:(id)sender;
- (IBAction)splitBookmarks:(id)sender;
- (IBAction)createFolder:(id)sender;

- (IBAction)showStatistics:(id)sender;

@end

