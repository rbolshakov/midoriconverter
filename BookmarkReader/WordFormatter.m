//
//  WordFormatter.m
//  BookmarkReader
//
//  Created by Nestline on 12/10/2016.
//
//

#import "WordFormatter.h"
#import "Word.h"

@implementation WordFormatter
- (nullable NSString *)stringForObjectValue:(nullable id)obj
{
    Word* word = (Word*)obj;
    return word.text;
}

- (nullable NSString *)editingStringForObjectValue:(id)obj
{
    Word* word = (Word*)obj;
    return word.text;
}
@end
