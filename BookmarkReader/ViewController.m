//
//  ViewController.m
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import "ViewController.h"
#import "Stats.h"
#import "DBOperator.h"
#import "BookmarkManager.h"
#import "FolderFormatter.h"

@interface ViewController()
{
    int totalWords;
}
@property (nonatomic, strong) NSMutableArray *fromItems;
@property (nonatomic, strong) NSMutableArray *toItems;
@property (nonatomic, strong) Folder* rootBookmark;
@property (nonatomic, strong) BookmarkManager* manager;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [[BookmarkManager alloc]init];
    self.rootBookmark = [[Folder alloc]init];
    self.rootBookmark.text = @"Bookmarks";
    self.outlineView.dataSource = self;
    self.outlineView.delegate = self;
    self.outlineView.allowsMultipleSelection = YES;
    self.logField.stringValue = @"";
    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:title];
    [alert setInformativeText:message];
    [alert addButtonWithTitle:@"OK"];
    [alert runModal];
}

- (void)activateInterface
{
    self.readButton.enabled = YES;
    self.writeButton.enabled = YES;
    self.importButton.enabled = YES;
    self.mergeButton.enabled = YES;
    self.substractButton.enabled = YES;
    self.insertButton.enabled = YES;
    self.splitButton.enabled = YES;
    self.deleteButton.enabled = YES;
    self.createButton.enabled = YES;
    self.fromButton.enabled = YES;
    self.toButton.enabled = YES;
    self.statsButton.enabled = YES;
}

- (void)deactivateInterface
{
    self.readButton.enabled = NO;
    self.writeButton.enabled = NO;
    self.importButton.enabled = NO;
    self.mergeButton.enabled = NO;
    self.substractButton.enabled = NO;
    self.insertButton.enabled = NO;
    self.splitButton.enabled = NO;
    self.deleteButton.enabled = NO;
    self.createButton.enabled = NO;
    self.fromButton.enabled = NO;
    self.toButton.enabled = NO;
    self.statsButton.enabled = NO;
}

#pragma mark Open/Save

- (IBAction)openFile:(id)sender
{
    NSString* openFilePath = [self openPathFromDialog];
    if (openFilePath)
    {
        NSString* bookmarks = [NSString stringWithContentsOfFile:openFilePath encoding:NSUTF8StringEncoding error:nil];
        [self deactivateInterface];
        [self.outlineView reloadData];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            MidoriBkParser* parser = [[MidoriBkParser alloc]init];
            parser.delegate = self;
            self.rootBookmark = [parser deserializeBookmarks:bookmarks];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.outlineView reloadData];
                [self activateInterface];
            });
        });
    }
}

- (IBAction)openImportFile:(id)sender
{
    NSString* openFilePath = [self openPathFromDialog];
    if (openFilePath)
    {
        NSString* csv = [NSString stringWithContentsOfFile:openFilePath encoding:NSUTF8StringEncoding error:nil];
        [self deactivateInterface];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CSVParser* parser = [[CSVParser alloc]init];
            parser.delegate = self;
            Folder* folder = [parser parseString: csv];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.rootBookmark addBookmark:folder];
                [self.outlineView reloadData];
                [self activateInterface];
            });
        });
    }
}

- (IBAction)saveFile:(id)sender
{
    NSString* saveFilePath = [self savePathFromDialog];
    if (saveFilePath)
    {
        MidoriBkParser* parser = [[MidoriBkParser alloc]init];
        NSString* bookmarks = [parser serializeBookmarks:self.rootBookmark];
        NSError* error;
        [bookmarks writeToFile:saveFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error)
            [self showAlertWithTitle:@"Save error" andMessage:@"Could not save bookmarks to file"];
    }
}

- (NSString*)openPathFromDialog
{
    NSArray *fileTypes = [NSArray arrayWithObjects:@"midori",nil];
    NSOpenPanel * panel = [NSOpenPanel openPanel];
    [panel setAllowsMultipleSelection:NO];
    [panel setCanChooseDirectories:NO];
    [panel setCanChooseFiles:YES];
    [panel setFloatingPanel:YES];
    NSInteger result = [panel runModal];
    panel.directoryURL = [NSURL URLWithString:NSHomeDirectory()];
    panel.allowedFileTypes = fileTypes;
    if(result == NSModalResponseOK)
    {
        NSArray* paths = [panel URLs];
        return [paths firstObject];
    }
    else
        return nil;
}

- (NSString*)savePathFromDialog
{
    NSArray *fileTypes = [NSArray arrayWithObjects:@"midori",nil];
    NSSavePanel * panel = [NSSavePanel savePanel];
    [panel setFloatingPanel:YES];
    [panel setCanCreateDirectories:YES];
    panel.directoryURL = [NSURL URLWithString:NSHomeDirectory()];
    panel.allowedFileTypes = fileTypes;
    NSInteger result = [panel runModal];
    if(result == NSModalResponseOK)
    {
        return panel.URL.path;
    }
    else
        return nil;
}

- (void)couldNotImportWords:(NSArray *)strings
{
    NSMutableString* message = [NSMutableString stringWithFormat:@"Could not import following words: \n"];
    for (NSString* word in strings)
    {
        [message appendString:word];
        [message appendString:@"\n"];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAlertWithTitle:@"Import Errors:" andMessage:message];
    });
}

- (IBAction)showStatistics:(id)sender
{
    Stats* stats = [[Stats alloc]initWithFolder:self.rootBookmark];
    NSString* message = [NSString stringWithFormat:@"%ld(%ld) слов, %ld(%ld) кандзи, %ld папок", stats.totalWords, stats.uniqueWords, stats.totalKanjis, stats.uniqueKanjis, stats.folders];
    self.logField.stringValue = message;
}


- (void)didProcessed:(NSInteger)words
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.logField.stringValue = [NSString stringWithFormat:@"Обработано: %ld слов", (long)words];
    });
}


#pragma mark Outline View data source

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(nullable id)item
{
    if ([item isKindOfClass:[Folder class]])
    {
        Folder* folder = (Folder*)item;
        return folder.items.count;
    }
    else if ([item isKindOfClass:[Word class]])
    {
        return 0;
    }
    else
        return 1;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(nullable id)item
{
    if ([item isKindOfClass:[Folder class]])
    {
        Folder* folder = (Folder*)item;
        return folder.items[index];
    }
    if ([item isKindOfClass:[Word class]])
    {
        return nil; //Seems it needed here
    }
    else
    {
        return self.rootBookmark;
    }
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    if ([item isKindOfClass:[Folder class]])
    {
        Folder* folder = (Folder*)item;
        return (folder.items.count > 0) ? YES : NO;
    }
    else
    {
        return NO;
    }
}

- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(id)cell forTableColumn:(nullable NSTableColumn *)tableColumn item:(id)item
{
    if([[tableColumn identifier]isEqualToString:@"text"])
    {
        if ([item isKindOfClass:[Folder class]])
        {
            FolderFormatter* formatter = [[FolderFormatter alloc]init];
            [cell setFormatter:formatter];
        }
        else
        {
            [cell setFormatter:nil];
        }
    }
}




- (nullable id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(nullable NSTableColumn *)tableColumn byItem:(nullable id)item
{
    if ([tableColumn.identifier isEqualToString:@"text"])
    {
        
        if ([item isKindOfClass:[Folder class]])
        {
            Folder* folder = (Folder*)item;
            return folder;
            return [NSString stringWithFormat:@"%@ (%ld) [%ld]", folder.text, folder.items.count, folder.totalWords];
        }
        else
        {
            Word* word = (Word*)item;
            return word.text;
        }
    }
    else if ([tableColumn.identifier isEqualToString:@"meaning"])
    {
        if ([item isKindOfClass:[Word class]])
        {
            Word* word = (Word*)item;
            return word.meaning;
        }
    }
    else
    {
        if ([item isKindOfClass:[Word class]])
        {
            Word* word = (Word*)item;
            return word.pronounciation;
        }
    }
    return @"";
}

- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(nullable id)object forTableColumn:(nullable NSTableColumn *)tableColumn byItem:(nullable id)item
{
    NSLog(@"%@", (NSString*)object);
    if ([item isKindOfClass:[Folder class]])
    {
        Folder* folder = (Folder*)item;
        Folder* newFolder = (Folder*)object;
        folder.text = newFolder.text;
    }
    else
    {
        Word* word = (Word*)item;
        NSString* newText = (NSString*)object;
        word.text = newText;
    }
}

#pragma mark Data selection

- (NSMutableArray*)arrayFromSelection
{
    NSIndexSet* set = self.outlineView.selectedRowIndexes;
    NSMutableArray* array = [NSMutableArray array];
    [set enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        Bookmark* item = [self.outlineView itemAtRow:idx];
        [array addObject:item];
    }];
    return array;
}

#pragma mark Actions

- (IBAction)selectFrom:(id)sender
{
    NSMutableArray* selection = [self arrayFromSelection];
    self.fromItems = selection;
    self.fromField.stringValue = [self.manager descriptionFromBookmarks:self.fromItems];
}

- (IBAction)selectTo:(id)sender
{
    NSMutableArray* selection = [self arrayFromSelection];
    self.toItems = selection;
    self.toField.stringValue = [self.manager descriptionFromBookmarks:self.toItems];
}

- (IBAction)insertBookmarks:(id)sender
{
    [self.manager insertBookmarks:self.fromItems toBookmarks:self.toItems];
    [self.outlineView reloadData];
    //[self.outlineView deselectAll:nil];
}

- (IBAction)mergeBookmarks:(id)sender
{
    [self.manager mergeBookmarks:self.fromItems toBookmarks:self.toItems];
    [self.outlineView reloadData];
    //[self.outlineView deselectAll:nil];
}

- (IBAction)substractBookmarks:(id)sender
{
    [self.manager substractBookmarks:self.fromItems fromBookmarks:self.toItems];
    [self.outlineView reloadData];
    //[self.outlineView deselectAll:nil];
}

- (IBAction)splitBookmarks:(id)sender
{
    NSMutableArray* selection = [self arrayFromSelection];
    [self.manager splitBookmarks:selection by:self.splitField.integerValue];
    [self.outlineView reloadData];
    [self.outlineView deselectAll:nil];
}

- (IBAction)deleteBookmarks:(id)sender
{
    NSMutableArray* selection = [self arrayFromSelection];
    [self.manager deleteBookmarks:selection];
    [self.outlineView reloadData];
    [self.outlineView deselectAll:nil];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(nullable NSTableColumn *)tableColumn item:(id)item
{
    if ([tableColumn.identifier isEqualToString:@"text"] && [item isKindOfClass:[Folder class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (IBAction)createFolder:(id)sender
{
    Bookmark* selectedItem = [self.outlineView itemAtRow:[self.outlineView selectedRow]];
    [self.manager createFolderInBookmark:selectedItem];
    [self.outlineView reloadData];
    [self.outlineView expandItem:selectedItem];
}

@end
