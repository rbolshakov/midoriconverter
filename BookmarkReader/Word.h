//
//  Word.h
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//
#import "Bookmark.h"
#import <Foundation/Foundation.h>
#import "Folder.h"

@interface Word : Bookmark<NSCopying>
@property (nonatomic, strong) NSString* code;
@property (nonatomic) BOOL isKanji;
@end
