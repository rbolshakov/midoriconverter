//
//  main.m
//  BookmarkReader
//
//  Created by Nestline on 05/10/2016.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
