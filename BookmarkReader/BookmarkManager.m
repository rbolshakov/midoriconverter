//
//  BookmarkManager.m
//  BookmarkReader
//
//  Created by Nestline on 11/10/2016.
//
//

#import "BookmarkManager.h"

@implementation BookmarkManager
#pragma mark Data processing

- (void)mergeBookmarks:(NSArray*)fromItems toBookmarks:(NSArray*)toItems
{
    for (Bookmark* firstBookmark in fromItems)
    {
        if ([firstBookmark isKindOfClass:[Folder class]])
        {
            for (Bookmark* secondBookmark in toItems)
            {
                if ([secondBookmark isKindOfClass:[Folder class]])
                {
                    [self mergeFolder:(Folder*)firstBookmark toFolder:(Folder*)secondBookmark];
                }
            }
        }
    }
}

- (void)substractBookmarks:(NSArray*)toItems fromBookmarks:(NSArray*)fromItems
{
    for (Bookmark* firstBookmark in fromItems)
    {
        if ([firstBookmark isKindOfClass:[Folder class]])
        {
            for (Bookmark* secondBookmark in toItems)
            {
                if ([secondBookmark isKindOfClass:[Folder class]])
                {
                    [self substractFolder:(Folder*)secondBookmark fromFolder:(Folder*)firstBookmark];
                }
            }
        }
    }
}

- (void)insertBookmarks:(NSArray*)fromItems toBookmarks:(NSArray*)toItems
{
    for (Bookmark* firstBookmark in fromItems)
    {
        for (Bookmark* secondBookmark in toItems)
        {
            if ([secondBookmark isKindOfClass:[Folder class]])
                [self insertBookmark:firstBookmark inFolder:(Folder*)secondBookmark];
        }
    }
}

- (void)splitBookmarks:(NSArray*)bookmarks by:(NSInteger)count
{
    for (Bookmark* bookmark in bookmarks)
    {
        if ([bookmark isKindOfClass:[Folder class]])
        {
            [self splitFolder:(Folder*)bookmark by:count];
        }
    }
}

- (void)deleteBookmarks:(NSArray*)bookmarks
{
    for (Bookmark* bookmark in bookmarks)
        [bookmark deleteIt];
}

- (void)mergeFolder:(Folder*)firstFolder toFolder:(Folder*)secondFolder
{
    for (Bookmark* item in firstFolder.items)
    {
        if (![self sameBookmark:item InFolder:secondFolder])
            [secondFolder addBookmark:[item copy]];
    }
}

- (void)substractFolder:(Folder*)secondFolder fromFolder:(Folder*)firstFolder
{
    for (Bookmark* item in secondFolder.items)
    {
        Bookmark* substractItem = [self sameBookmark:item InFolder:firstFolder];
        if (substractItem)
            [substractItem deleteIt];
    }
}

- (void)insertBookmark:(Bookmark*)bookmark inFolder:(Folder*)folder
{
    if (![self sameBookmark:bookmark InFolder:folder])
        [folder addBookmark:[bookmark copy]];
}

- (void)splitFolder:(Folder*)folder by:(NSInteger)count
{
    int num = 1;
    NSInteger current = 0;
    while (current < folder.items.count)
    {
        Folder* split = [[Folder alloc]init];
        [folder.parent addBookmark:split];
        split.text = [NSString stringWithFormat:@"%@ %d", folder.text, num];
        [self copyBookmarksFromFolder:folder toFolder:split inRange:NSMakeRange(current, count)];
        num++;
        current += count;
    }
}

- (void)copyBookmarksFromFolder:(Folder*)firstFolder toFolder:(Folder*)secondFolder inRange:(NSRange)range
{
    for (NSInteger i = range.location; i < range.location + range.length && i < firstFolder.items.count; i++)
    {
        Bookmark* bookmark = firstFolder.items[i];
        [secondFolder addBookmark:[bookmark copy]];
    }
}

- (void)createFolderInBookmark:(Bookmark*)bookmark
{
    if ([bookmark isKindOfClass:[Folder class]])
    {
        Folder* folder = (Folder*)bookmark;
        Folder* subFolder = [[Folder alloc]init];
        subFolder.text = @"New";
        [folder addBookmark:subFolder];
    }
}

- (Bookmark*)sameBookmark:(Bookmark*)bookmark InFolder:(Folder*)parent
{
    for (Bookmark* other in parent.items)
    {
        if ([bookmark isEqual:other])
            return other;
    }
    return NO;
}

- (NSString*)descriptionFromBookmarks:(NSMutableArray*)bookmarks
{
    NSMutableString* string = [NSMutableString string];
    for (int i = 0; i < bookmarks.count; i++)
    {
        Bookmark* bookmark = bookmarks[i];
        if (i < 10)
        {
            if (i != 0)
                [string appendString:@", "];
            [string appendString:bookmark.text];
        }
        else if (i == 10)
            [string appendString:@"..."];
    }
    return string;
}

@end
